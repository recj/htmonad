import Tag

data Attr
  = Src String
  | Href String
  | Alt String
  | Title String
  | Width String
  | Height String
  | Id String
  | Disable Bool

data Attr
= Src String
| Href String
| Alt String
| Title String
| Width String
| Height String
| Id String
| Disable Bool

type HTML = [(Tag, [Attr])]

displayTag :: Tag -> (String, [Attr], HTML)
displayTag (Img src) = ("img", [Str src], [])
displayTag (A href body) = ("a", [Href href], body)

haaaa :: Tag -> Bool
haaaa (Img _) = False
haaaa _ = True

displayHTML :: HTML -> String
displayHTML = foldl (\(tag, attr) -> case (haaaa tag, displayTag tag) of
                                       (False, (s, attr2, _))
                                         -> "<"
                                            ++ s
                                            ++ " "
                                            ++ (displayAttr $ attr2 ++ attr)
                                            ++ " />"
                                       (_, (s, attr2, html))
                                         -> "<"
                                            ++ s
                                            ++ " "
                                            ++ (displayAttr $ attr2 ++ attr)
                                            ++ ">"
                                            ++ displayHTML html
                                            ++ "</"
                                            ++ s
                                            ++ ">") ""

tag :: Tag -> HTML
tag x = [(x, [])]

mkPage :: String -> HTML -> HTML -> String
mkPage title header body = displayHTML [tag Html, tag $ Header $ (tag $ Title title):header, tag $ Body body]

butts
  = mkPage "Butt page" []
    $ [tag $ H1 "Butt page"
      ,tag $ Img]
