data Tag
= A
| Abbr
| Address
| Area
| Article
| Aside
| Audio
| B
| Base
| Bdi
| Bdo
| Blockquote
| Body
| Br
| Button
| Canvas
| Caption
| Cite
| Code
| Col
| Colgroup
| Comment
| Data
| Datalist
| Dd
| Del
| Details
| Dfn
| Dialog
| Div
| Dl
| Doctype
| Dt
| Em
| Embed
| Fieldset
| Figcaption
| Figure
| Footer
| Form
| H Int
| Head
| Header
| Hr
| Html
| I
| IFrame
| Img
| Input
| Ins
| Kbd
| Label
| Legend
| Li
| Link
| Main
| Map
| Mark
| Meta
| Meter
| Nav
| Noscript
| Object
| Ol [Li]
| Optgroup
| Option
| Output
| P
| Param
| Picture
| Pre
| Progress
| Q
| Rp
| Rt
| Ruby
| S
| Samp
| Script
| Section
| Select
| Small
| Source
| Span
| Strong
| Style
| Sub
| Summary
| Sup
| Svg
| Table
| Tbody
| Td
| Template
| Textarea
| Tfoot
| Th
| Thead
| Time
| Title
| Tr
| Track
| U
| Ul
| Var
| Video
| Wbr

instance Show Tag where
| show (A _ _) = "a"
| show Abbr = "abbr"
| show Address = "address"
| show Area = "area"
| show Article = "article"
| show Aside = "aside"
| show Audio = "audio"
| show B = "b"
| show Base = "base"
| show Bdi = "bdi"
| show Bdo = "bdo"
| show Blockquote = "blockquote"
| show Body = "body"
| show Br = "br"
| show Button = "button"
| show Canvas = "canvas"
| show Caption = "caption"
| show Cite = "cite"
| show Code = "code"
| show Col = "col"
| show Colgroup = "colgroup"
| show Comment = "comment"
| show Data = "data"
| show Datalist "datalist"
| show Dd = "dd"
| show Del = "del"
| show Details = "details"
| show Dfn = "dfn"
| show Dialog = "dialog"
| show Div = "div"
| show Dl = "dl"
| show Doctype = "doctype"
| show Dt = "dt"
| show Em = "em"
| show Embed = "embed"
| show Fieldset = "fieldset"
| show Figcaption = "figcaption"
| show Figure = "figure"
| show Footer = "footer"
| show Form = "form"
| show (H i) = "h" ++ show i
| show Head = "head"
| show Header = "header"
| show Hr = "hr"
| show Html = "html"
| show I = "i"
| show IFrame = "iframe"
| show Img = "img"
| show Input = "input"
| show Ins = "ins"
| show Kbd = "kbd"
| show Label = "label"
| show Legend = "legend"
| show Li = "li"
| show Link = "link"
| show Main = "main"
| show Map = "map"
| show Mark = "mark"
| show Meta = "meta"
| show Meter = "meter"
| show Nav = "nav"
| show Noscript = "noscript"
| show Object = "object"
| show Ol [Li] = "ol"
| show Optgroup = "optgroup"
| show Option = "option"
| show Output = "output"
| show P = "p"
| show Param = "param"
| show Picture = "picture"
| show Pre = "pre"
| show Progress = "progress"
| show Q = "q"
| show Rp = "rp"
| show Rt = "rt"
| show Ruby = "ruby"
| show S = "s"
| show Samp = "samp"
| show Script = "script"
| show Section = "section"
| show Select = "select"
| show Small = "small"
| show Source = "source"
| show Span = "span"
| show Strong = "strong"
| show Style = "style"
| show Sub = "sub"
| show Summary = "summary"
| show Sup = "sup"
| show Svg = "svg"
| show Table = "table"
| show Tbody = "tbody"
| show Td = "td"
| show Template = "template"
| show Textarea = "textarea"
| show Tfoot = "tfoot"
| show Th = "th"
| show Thead = "thead"
| show Time = "time"
| show Title = "title"
| show Tr = "tr"
| show Track = "track"
| show U = "u"
| show Ul = "ul"
| show Var = "var"
| show Video = "video"
| show Wbr = "wbr"

isWrappable :: Tag -> Bool =
isWrappable Area = False
isWrappable Base = False
isWrappable Br = False
isWrappable Hr = False
isWrappable Col = False
isWrappable Command = False
isWrappable Embed = False
isWrappable Hr = False
isWrappable Img = False
isWrappable Input = False
isWrappable Link = False
isWrappable Meta = False
isWrappable Param = False
isWrappable Source = False
isWrappable Track = False
isWrappble Wbr = False
isWrappable _ = True

wrapTag t contents =
  if isWrappable t then
     "<" ++ (show t) ++ ">" ++ contents ++ "</" ++ (show t) ++ >"
  else
    "<" ++ (show t) ++ ">"
